Dotfiles
=======

# List
- [x] zsh
- [x] vim
- [x] tmux
- [ ] rofi
- dconf / gsettings:
    - [ ] keybindings
    - [x] gnome-terminal

